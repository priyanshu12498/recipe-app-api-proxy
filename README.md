# Recipe App Api Proxy

NGINX Proxy app for our recipe app API to serve the static files

## Usage

### Env Variables

 * 'LISTEN_PORT' - Port to listen on (default: '8000')
 * 'APP_HOST' - Hostname of the app to forward request to (default: 'app')
 * 'APP_PORT' -  port of the app to forward requests to (default: '9000')